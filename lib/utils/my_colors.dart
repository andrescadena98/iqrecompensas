import 'package:flutter/cupertino.dart';
import 'package:hexcolor/hexcolor.dart';

class MyColors {
  static Color colorPrincipalAZ = HexColor('#1C3456');
  static Color colorVerd =  HexColor("#48A990");
    static Color colorTextField = Color.fromRGBO(0, 43, 92, 0.09);
  static Color colorTextFieldDark = Color.fromRGBO(175, 51, 255, 0.09);
 
}
