// ignore_for_file: prefer_const_constructors

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_calendar/flutter_advanced_calendar.dart';

import 'package:qrecompensas/utils/my_colors.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class DetallePage extends StatefulWidget {
  int indexCredito;
  DetallePage({Key? key, required this.indexCredito}) : super(key: key);

  @override
  State<DetallePage> createState() => _DetallePageState();
}

class _DetallePageState extends State<DetallePage> {
  final _controller = AdvancedCalendarController.today();

  //eventos Calendario
  final List<DateTime> events = [
    DateTime.parse('2022-02-03'),
    DateTime.parse('2022-02-04'),
  ];

  @override
  Widget build(BuildContext context) {
    final cantidadCredito = 10000;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Detalle',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        body: Center(
          child: ListView(
            children: [
              
              SlideInLeft(
                child: Container(
                  margin: EdgeInsets.only(top: 30),
                  child: SleekCircularSlider(
                    appearance: CircularSliderAppearance(
                        size: 200,
                        customColors: CustomSliderColors(
                          progressBarColor: MyColors.colorPrincipalAZ,
                          dotColor: Colors.white,
                          trackColor: Colors.black,
                        ),
                        customWidths: CustomSliderWidths(progressBarWidth: 10)),
                    min: 10,
                    max: 28,
                    initialValue: 14,
                  ),
                ),
              ),
              
              SizedBox(height: 30,),
              SlideInRight(
                child: Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 150,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 160,
                          child: Card(
                            color: MyColors.colorVerd,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Monto a pagar',
                                    style: TextStyle(
                                        fontSize: 15.0, color: Colors.white),
                                  ),
                                  Spacer(),
                                  Text(
                                    '\$${cantidadCredito}',
                                    style: TextStyle(
                                        fontSize: 30.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  Spacer(),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 160,
                          child: Card(
                            color: MyColors.colorVerd,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Avance',
                                    style: TextStyle(
                                        fontSize: 15.0, color: Colors.white),
                                  ),
                                  Spacer(),
                                  Text(
                                    '\$${(cantidadCredito * 0.14).round()}',
                                    style: TextStyle(
                                        fontSize: 30.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  Spacer(),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              
              SlideInLeft(
                child: Container(
                  margin: EdgeInsets.only(top: 40,bottom: 20),
                  child: Center(
                    child: Text('Próximos Pagos',style: TextStyle(fontSize: 20,fontFamily: 'Monserrat',fontWeight: FontWeight.bold),),
                  ),
                ),
              ),
              SlideInLeft(
                child: AdvancedCalendar(
                  
                  todayStyle: TextStyle(color: MyColors.colorVerd),
                  events: events,
                  weekLineHeight: 48.0,
                  startWeekDay: 1,
                  controller: _controller,
                ),
              ),
              SlideInRight(
                child: Container(
                  margin: EdgeInsets.only(top: 40,bottom: 20),
                  child: Center(
                    child: Text('Pagos Pendientes',style: TextStyle(fontSize: 20,fontFamily: 'Monserrat',fontWeight: FontWeight.bold),),
                  ),
                ),
              ),
              SlideInRight(
                child: Container(
                        margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 18.0),
                        height: 80.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12.0),
                          color: MyColors.colorPrincipalAZ,
                          boxShadow: const [
                BoxShadow(
                  offset: Offset(3, 5),
                  color: Colors.black12,
                  blurRadius: 10,
                ),
                BoxShadow(
                  offset: Offset(-1, -1),
                  color: Colors.white,
                  blurRadius: 6,
                ),
                          ],
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                Icon(
                  Icons.monetization_on,
                  color: Colors.white,
                ),
                const SizedBox(width: 15.0),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const <Widget>[
                    Text(
                      'Urgente',
                      style:
                          TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600,color: Colors.white),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Text(
                      '23 Feb, 8:36 PM',
                      style: TextStyle(
                          fontSize: 12.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                  ],
                ),
                const Spacer(),
                Text('\$100',
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.red)),
                          ],
                        ),
                      ),
              ),
             
            ],
          ),
        ));
  }
}
