import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';


// Funcionalidad de toggle en el icono menu
class MenuWidget extends StatelessWidget {
  const MenuWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>IconButton(
    icon: Icon(Icons.menu,color: Colors.black,),
    onPressed: ()=>ZoomDrawer.of(context)!.toggle(),
  );
}