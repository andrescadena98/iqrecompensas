import 'dart:io';

import 'package:animated_floating_buttons/animated_floating_buttons.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:file_picker_patch/file_picker_patch.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:flutter_arc_speed_dial/flutter_speed_dial_menu_button.dart';
import 'package:flutter_arc_speed_dial/main_menu_floating_action_button.dart';
import 'package:qrecompensas/services/usersInfo_services.dart';
import 'package:qrecompensas/utils/my_colors.dart';
import 'package:qrecompensas/widgets/my_cards_view.dart';
import 'package:qrecompensas/widgets/notification.dart';
import 'package:qrecompensas/widgets/recent_transaction_view.dart';
import 'package:share_plus/share_plus.dart';
import 'package:whatsapp_share2/whatsapp_share2.dart';

class HomePage extends StatefulWidget {
  static final String routeName = 'home';
  HomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>with SingleTickerProviderStateMixin {
  final _advancedDrawerController = AdvancedDrawerController();
  final userServices = UsersServices();
  

  //Iniciales para las notificaciones
  String notificationTitle = 'No Title';
  String notificationBody = 'No Body';
  String notificationData = 'No Data';
  @override
  void initState() {
    super.initState();
    final firebaseMessaging = FCM();
    firebaseMessaging.setNotifications();
    firebaseMessaging.streamCtlr.stream.listen(_changeData);
    firebaseMessaging.bodyCtlr.stream.listen(_changeBody);
    firebaseMessaging.titleCtlr.stream.listen(_changeTitle);
    


    final token = FirebaseMessaging.instance
        .getToken()
        .then((value) => print('Token: $value'));
        
  }

//recibe los argumentos pricipales de la notificacion
  _changeData(String msg) => setState(() => notificationData = msg);
  _changeBody(String msg) => setState(() => CoolAlert.show(
      lottieAsset: 'assets/images/notification.json',
      backgroundColor: Colors.white,
      context: context,
      type: CoolAlertType.success,
      text: notificationBody = msg,
      confirmBtnText: 'Ok',
      onConfirmBtnTap: () {
        Navigator.pop(context);
      }));
  _changeTitle(String msg) => setState(() => notificationTitle = msg);

  //Compartir text WS

  Future<void> share() async {
    await WhatsappShare.share(
      text: 'Whatsapp share text',
      linkUrl: 'https://flutter.dev/',
      phone: '5636137508s',
    );
  }

  final GlobalKey<AnimatedFloatingActionButtonState> key =
      GlobalKey<AnimatedFloatingActionButtonState>();

  /// and then assign it to the our widget library




bool _isShowDial = false;

  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
      backdropColor: MyColors.colorPrincipalAZ,
      controller: _advancedDrawerController,
      animationCurve: Curves.easeInOut,
      animationDuration: const Duration(milliseconds: 300),
      animateChildDecoration: true,
      rtlOpening: false,
      disabledGestures: false,
      childDecoration: const BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(16)),
      ),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Mis Créditos',
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.black,
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            color: Colors.black,
            onPressed: _handleMenuButtonPressed,
            icon: ValueListenableBuilder<AdvancedDrawerValue>(
              valueListenable: _advancedDrawerController,
              builder: (_, value, __) {
                return AnimatedSwitcher(
                  duration: Duration(milliseconds: 250),
                  child: Icon(
                    value.visible ? Icons.clear : Icons.person,
                    key: ValueKey<bool>(value.visible),
                  ),
                );
              },
            ),
          ),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: InkWell(
                  child: MyCardsView(),
                  onTap: () {},
                ),
              ),
              Expanded(
                flex: 1,
                child: RecentTransaction(),
              ),
            ],
          ),
        ),
        floatingActionButton:  _getFloatingActionButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      ),
      drawer: SafeArea(
        child: Container(
          child: ListTileTheme(
            textColor: Colors.white,
            iconColor: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                    width: 128.0,
                    height: 128.0,
                    margin: const EdgeInsets.only(
                      top: 24.0,
                      bottom: 64.0,
                    ),
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                      color: Colors.black26,
                      shape: BoxShape.circle,
                    ),
                    child: Image.asset('assets/images/usuario.png')),
                ListTile(
                    onTap: () {},
                    title: Text(
                      'Perfil',
                      style: TextStyle(
                          fontSize: 30,
                          fontFamily: 'Monserrat',
                          fontWeight: FontWeight.bold),
                    )),
                ListTile(
                    onTap: () {},
                    leading: Icon(Icons.person),
                    title: Text('José Andrés Alanís Cadena')),
                ListTile(
                    leading: Icon(Icons.vpn_key_rounded),
                    onTap: () {},
                    title: Text('310')),
                ListTile(
                    leading: Icon(Icons.work),
                    onTap: () {},
                    title: Text('Sistemas')),
                Spacer(),
                DefaultTextStyle(
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white54,
                  ),
                  child: Container(
                    margin: const EdgeInsets.symmetric(
                      vertical: 16.0,
                    ),
                    child: Container(
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 50, vertical: 10),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, 'login');
                        },
                        child: const Text(
                          'Cerrar Sesión',
                          style: TextStyle(color: Colors.black),
                        ),
                        style: ElevatedButton.styleFrom(
                            shadowColor: Colors.black,
                            primary: Colors.white,
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            padding: const EdgeInsets.symmetric(vertical: 15)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleMenuButtonPressed() {
    // NOTICE: Manage Advanced Drawer state through the Controller.
    // _advancedDrawerController.value = AdvancedDrawerValue.visible();
    _advancedDrawerController.showDrawer();
  }


  //Menu shared
   Widget _getFloatingActionButton() {
    return SpeedDialMenuButton(
      //if needed to close the menu after clicking sub-FAB
      isShowSpeedDial: _isShowDial,
      //manually open or close menu
      updateSpeedDialStatus: (isShow) {
        //return any open or close change within the widget
        this._isShowDial = isShow;
      },
      //general init
      isMainFABMini: false,
      mainMenuFloatingActionButton: MainMenuFloatingActionButton(
          mini: false,
          backgroundColor: MyColors.colorVerd,
          child: Icon(Icons.share),
          onPressed: () {},
          closeMenuChild: Icon(Icons.close),
          closeMenuForegroundColor: Colors.white,
          closeMenuBackgroundColor: Colors.red),
      floatingActionButtonWidgetChildren: <FloatingActionButton>[
        //Hijos Menu
        FloatingActionButton(
          mini: false,
          child: Icon(Icons.text_format),
          onPressed: () {
            share();
            _isShowDial = false;
            setState(() {});
          },
          backgroundColor: MyColors.colorVerd,
        ),
        FloatingActionButton(
          mini: false,
          child: Icon(Icons.image),
          onPressed: ()async {
             FilePickerResult? result = await FilePicker.platform.pickFiles();

          if (result != null) {
            File file = File(result.files.single.path.toString());
            CoolAlert.show(
                showCancelBtn: true,
                lottieAsset: 'assets/images/send.json',
                backgroundColor: Colors.white,
                context: context,
                type: CoolAlertType.success,
                widget: Image.file(file),
                text: "Imagen a enviar: ",
                confirmBtnText: 'Enviar',
                cancelBtnText: 'Cerrar',
                onCancelBtnTap: () {
                  Navigator.pop(context);
                },
                onConfirmBtnTap: () {
                  Share.shareFiles(['${file.path.toString()}'],
                      text: 'Great picture');
                });
          } else {
            // User canceled the picker
          }
            _isShowDial = !_isShowDial;
            setState(() {});
          },
          backgroundColor: MyColors.colorVerd,
        ),
        
      ],
      isSpeedDialFABsMini: true,
      paddingBtwSpeedDialButton: 30.0,
    );
  }

  Widget _getBodyWidget() {
    return Container();
  }
}
