import 'package:flutter/material.dart';


class OnboardingStep extends StatelessWidget {
  const OnboardingStep({
    Key? key,
    required this.image,
    required this.title,
    required this.subtitle, 
    
  }) : super(key: key);

  final String image;
  final String title;
  final String subtitle;
  

  final TextStyle titleStyle = const TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 20.0,
    
  );
  final TextStyle subtitleStyle = const TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.w300,
    
  );

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Container(
      
      child: Column(
        children: [
          SizedBox(height: 30.0),
          Image(
            image: AssetImage(image),
            width: size.width * 0.9,
            fit: BoxFit.contain,
          ),
          SizedBox(height: 50.0),
          Container(
            width: size.width * 0.8,
            child: Text(
              title,
              style: titleStyle,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            width: size.width * 0.8,
            child: Text(
              subtitle,
              style: subtitleStyle,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
