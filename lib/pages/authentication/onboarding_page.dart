import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:qrecompensas/pages/Principal/homePage.dart';
import 'package:qrecompensas/pages/authentication/onboarding_step.dart';
import 'package:qrecompensas/providers/onboarding_provider.dart';
import 'package:qrecompensas/utils/my_colors.dart';

import 'package:qrecompensas/utils/utils.dart';


// ignore: use_key_in_widget_constructors
class OnboardingPage extends StatelessWidget {
  static const String routeName = 'onboarding';

  @override
  Widget build(BuildContext context) {
   
    var onboardingProvider = Provider.of<OnboardingProvider>(context);
    var screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        children: [
          const _OnboardingPages(),
          const Align(
            alignment: Alignment.bottomCenter,
            child: _Dots(),
          ),
          Positioned(
            right: 0,
            bottom: screenHeight * 0.05,
            child: SizedBox(
         width: 100,
          child: ElevatedButton(
            onPressed: ()  {
             if (onboardingProvider.currentPage == 2) {
                

                  Navigator.of(context)
                      .pushReplacementNamed('login');
                }

                onboardingProvider.currentPage =
                    onboardingProvider.currentPage + 1;
            },
            child:const Text('Siguiente'),
            style: ElevatedButton.styleFrom(
               elevation: 4,
                primary: MyColors.colorPrincipalAZ,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                padding:const  EdgeInsets.symmetric(vertical: 15)),
          ),
        ),
          ),
        ],
      ),
    );
  }
}

class _OnboardingPages extends StatelessWidget {
  const _OnboardingPages({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var onboardingProvider = Provider.of<OnboardingProvider>(context);

    return PageView(
      physics: const NeverScrollableScrollPhysics(),
      controller: onboardingProvider.pageController,
      onPageChanged: (int index) {
        onboardingProvider.currentPage = index;
      },
      children: const [
        OnboardingStep(
          image: 'assets/images/man.png',
          title: 'Decentralized Ridesharing',
          subtitle:
              '0% commission platform - drivers can earn significantly more, while riders can catch a ride for substantially less.',
          
        ),
        OnboardingStep(
          image: 'assets/images/project.png',
          title: 'NEO Blockchain',
          subtitle:
              'NEO provides a fast, highly-scalable and modern blockchain infrastructure used to enable to CRUZEO Platform.',
         
        ),
        OnboardingStep(
          image: 'assets/images/dollars.png',
          title: 'Ontology Framework',
          subtitle:
              'CRUZEO establishes a distributed trust network with the Ontology framework for driver digital identity certification.',
          
        ),
      ],
    );
  }
}

class _Dots extends StatelessWidget {
  const _Dots({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var onboardingProvider = Provider.of<OnboardingProvider>(context);
    var screenHeight = MediaQuery.of(context).size.height;

    return Container(
      margin: EdgeInsets.only(bottom: screenHeight * 0.2),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _Dot(active: onboardingProvider.currentPage == 0),
          const SizedBox(width: 10.0),
          _Dot(active: onboardingProvider.currentPage == 1),
          const SizedBox(width: 10.0),
          _Dot(active: onboardingProvider.currentPage == 2),
        ],
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  const _Dot({
    Key? key,
    required this.active,
  }) : super(key: key);

  final bool active;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: active ? CustomTheme.colorGreen : CustomTheme.colorSpanishGray,
        borderRadius: BorderRadius.circular(5.0),
      ),
      width: 10.0,
      height: 10.0,
    );
  }
}
