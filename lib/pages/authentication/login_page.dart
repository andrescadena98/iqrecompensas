

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:qrecompensas/pages/Principal/homePage.dart';
import 'package:qrecompensas/utils/my_colors.dart';
import 'package:local_auth/local_auth.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController curpController =  TextEditingController();
  TextEditingController passwordControl =  TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  //Finger
   bool? _hasBiometricSenson;
  // list of finger print added in local device settings
   List<BiometricType>? _availableBiomatrics;
  String  _isAuthorized = "NOT AUTHORIZED";
  LocalAuthentication authentication = LocalAuthentication();


  ////future function to check is the use is authorized or no
  Future<void> _getAuthentication() async{
    bool isAutherized = false;
    try{
      isAutherized = await authentication.authenticate(
          localizedReason: "SCAN YOUR FINGER PRINT TO GET AUTHORIZED",
          useErrorDialogs: true,
          stickyAuth: false
      );
      if(isAutherized=true){
         Navigator.pushNamed(context, HomePage.routeName);
      }
    } on PlatformException catch(e)
    {
      print(e);
    }
    
    
     
    
  }

  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: Stack(children: [
            Positioned(
              child: circleLogin(),
              
            ),
           
            SingleChildScrollView(
              child: Column(
                children: [
                  Image.asset('assets/images/splash_screen.png',width: 150,height: 150,),
                  repartidorAnimado(),
                  nombreEmpresa(),
                  inputDatosUser(),
                  loginFooter()
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  // Imagen login Animada
  Widget repartidorAnimado() {
    return Lottie.asset('assets/images/loan.json', width: 250, height: 250);
  }

  //Text Login


  //Texto Login En esquina superior
  Widget circleLogin() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 400,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
          color: MyColors.colorPrincipalAZ),
    );
  }

  Widget nombreEmpresa() {
    return Container(
      margin: const EdgeInsets.all(10),
      child:const Text('Recompensas',
          style: TextStyle(
            fontFamily:'Montserrat',
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 32,
          )),
    );
  }

 

  // TextForm - Inputs para la parte de login
  Widget inputDatosUser() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 30),
            decoration: BoxDecoration(
                color: MyColors.colorTextField,
                borderRadius: BorderRadius.circular(30)),
            child: TextField(
              controller: curpController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  hintText: 'CURP',
                  border: InputBorder.none,
                  contentPadding:const EdgeInsets.all(15),
                  prefixIcon: Icon(
                    Icons.person,
                    color: MyColors.colorPrincipalAZ,
                  ),
                  hintStyle: TextStyle(color:MyColors.colorPrincipalAZ)),
            ),
          ),
          Container(
            margin:const EdgeInsets.symmetric(horizontal: 50, vertical: 0),
            decoration: BoxDecoration(
                color: MyColors.colorTextField,
                borderRadius: BorderRadius.circular(30)),
            child: TextField(
              controller: passwordControl,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  hintText: 'Contraseña',
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.all(15),
                  prefixIcon: Icon(
                    Icons.lock,
                    color: MyColors.colorPrincipalAZ,
                  ),
                  hintStyle: TextStyle(color: MyColors.colorPrincipalAZ)),
            ),
          ),
        ],
      ),
    );
  }

  Widget loginFooter() {
    return Column(
      children: [
        Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 30),
          child: ElevatedButton(
            onPressed: ()  {
              _getAuthentication();
            },
            child:const Text('Iniciar Sesión'),
            style: ElevatedButton.styleFrom(
               elevation: 4,
                primary: MyColors.colorPrincipalAZ,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                padding:const  EdgeInsets.symmetric(vertical: 15)),
          ),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
          child: ElevatedButton(
            onPressed: ()  {
              Navigator.pushNamed(context, 'register');
            },
            child:const Text('Registrarse',style: TextStyle(color: Colors.black),),
            style: ElevatedButton.styleFrom(
                shadowColor: Colors.black,

                primary: Colors.white,
                elevation: 4,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                padding:const  EdgeInsets.symmetric(vertical: 15)),
          ),
        ),
        
      
      ],
    );
  }
}



showAlertDialog(BuildContext context, data) {
  Widget okButton = TextButton(
    child: const Text("Aceptar"),
    onPressed: () {
      Navigator.pop(context);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: const Text('Error'),
    content: Text(data),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}


