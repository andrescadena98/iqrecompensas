import 'package:flutter/material.dart';
import 'package:qrecompensas/utils/my_colors.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController curpController = TextEditingController();
  TextEditingController passwordControl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.colorPrincipalAZ,
        appBar: AppBar(
          backgroundColor: MyColors.colorPrincipalAZ,
          iconTheme: IconThemeData(color: Colors.white),
          elevation: 0,
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Image.asset('assets/images/splash_screen.png'),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: SingleChildScrollView(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                decoration: BoxDecoration(
                                    color: MyColors.colorTextField,
                                    borderRadius: BorderRadius.circular(30)),
                                child: TextField(
                                  controller: curpController,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      hintText: 'CURP',
                                      border: InputBorder.none,
                                      contentPadding: const EdgeInsets.all(15),
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: MyColors.colorPrincipalAZ,
                                      ),
                                      hintStyle: TextStyle(
                                          color: MyColors.colorPrincipalAZ)),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                decoration: BoxDecoration(
                                    color: MyColors.colorTextField,
                                    borderRadius: BorderRadius.circular(30)),
                                child: TextField(
                                  controller: passwordControl,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      hintText: 'No. Contrato',
                                      border: InputBorder.none,
                                      contentPadding: const EdgeInsets.all(15),
                                      prefixIcon: Icon(
                                        Icons.lock,
                                        color: MyColors.colorPrincipalAZ,
                                      ),
                                      hintStyle: TextStyle(
                                          color: MyColors.colorPrincipalAZ)),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                decoration: BoxDecoration(
                                    color: MyColors.colorTextField,
                                    borderRadius: BorderRadius.circular(30)),
                                child: TextField(
                                  controller: passwordControl,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      hintText: 'Contraseña',
                                      border: InputBorder.none,
                                      contentPadding: const EdgeInsets.all(15),
                                      prefixIcon: Icon(
                                        Icons.lock,
                                        color: MyColors.colorPrincipalAZ,
                                      ),
                                      hintStyle: TextStyle(
                                          color: MyColors.colorPrincipalAZ)),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                decoration: BoxDecoration(
                                    color: MyColors.colorTextField,
                                    borderRadius: BorderRadius.circular(30)),
                                child: TextField(
                                  controller: passwordControl,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      hintText: 'Confirmar Contraseña',
                                      border: InputBorder.none,
                                      contentPadding: const EdgeInsets.all(15),
                                      prefixIcon: Icon(
                                        Icons.lock,
                                        color: MyColors.colorPrincipalAZ,
                                      ),
                                      hintStyle: TextStyle(
                                          color: MyColors.colorPrincipalAZ)),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  registerFooter()
                ],
              ),
            ),
          ),
        ));
  }

  Widget registerFooter() {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 40),
      child: ElevatedButton(
        onPressed: () {
          Navigator.pushNamed(context, 'register');
        },
        child: const Text(
          'Registrarse',
          style: TextStyle(color: Colors.black),
        ),
        style: ElevatedButton.styleFrom(
            shadowColor: Colors.black,
            primary: Colors.white,
            elevation: 4,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30)),
            padding: const EdgeInsets.symmetric(vertical: 15)),
      ),
    );
  }
}
