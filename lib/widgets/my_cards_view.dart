// ignore_for_file: prefer_const_constructors

import 'package:animate_do/animate_do.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:qrecompensas/pages/Principal/detalle_page.dart';
import 'package:qrecompensas/utils/my_colors.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

// ignore: use_key_in_widget_constructors
class MyCardsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FadeInDown(
      child: Swiper(
        pagination:  SwiperPagination(
                  alignment: Alignment.bottomCenter,
                  builder:  DotSwiperPaginationBuilder(
                      color: Colors.grey, activeColor: Color(0xff38547C)),
                ),
                
        itemCount: 4,
        itemBuilder: (BuildContext context, int index){
          return GestureDetector(
            onTap: (){
              
               Navigator.push( context, MaterialPageRoute(builder: (context) => DetallePage(indexCredito: index)), );
            },
            child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
                  alignment: Alignment.topLeft,
                  child: Column(
            
            children: <Widget>[
              
              SizedBox(
                height: 30.0,
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(30.0),
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          // ignore: prefer_const_literals_to_create_immutables
                          children: <Widget>[
                            Text(
                              'Estatus ••••',
                              style: TextStyle(fontSize: 17.0, color: Colors.white),
                            ),
                            SizedBox(
                              width: 8.0,
                            ),
                            Text(
                              'Activo',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                            Spacer(),
                            Text(
                              'Q',
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )
                          ],
                        ),
                        Spacer(),
                        Center(
                          child: SleekCircularSlider(
                            appearance: CircularSliderAppearance(
                                customColors: CustomSliderColors(
                                  progressBarColor: MyColors.colorPrincipalAZ,
                                  dotColor: Colors.white,
                                  trackColor: Colors.white,
                                ),
                                customWidths:
                                    CustomSliderWidths(progressBarWidth: 10)),
                            min: 10,
                            max: 28,
                            initialValue: 14,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // ignore: prefer_const_literals_to_create_immutables
                          children: <Widget>[
                            Text(
                              'Cantidad',
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white54),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              '\$4000',
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            
                          ],
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30.0),
                    color: MyColors.colorVerd,
                    // ignore: prefer_const_literals_to_create_immutables
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(8, 16),
                        color: Colors.black12,
                        blurRadius: 6,
                      ),
                      BoxShadow(
                        offset: Offset(-2, -3),
                        color: Colors.white,
                        blurRadius: 6,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30.0),
            ],
                  ),
                ),
          );
        },
        ),
    );
  }

  
}
