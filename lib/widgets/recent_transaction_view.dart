import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:qrecompensas/models/transaction.dart';
import 'package:qrecompensas/utils/my_colors.dart';


class RecentTransaction extends StatelessWidget {
  final List<Transaction> recentTransactions =
      Transaction.getRecentTransactions();

   RecentTransaction({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SlideInRight(
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(
                'Pagos Recientes',
                style: TextStyle(fontSize: 19.0, fontWeight: FontWeight.w600),
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Expanded(
            child: FadeInUp(
              child: ListView.builder(
                  itemCount: recentTransactions.length,
                  itemBuilder: (context, index) =>
                      _buildTransactionView(context, recentTransactions[index])),
            ),
          ),
        ],
      ),
    );
  }

  InkWell _buildTransactionView(BuildContext context, Transaction recentTransaction) =>
      InkWell(
        onTap: () {
          
        },
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 18.0),
          height: 80.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            color: MyColors.colorPrincipalAZ,
            boxShadow: const [
              BoxShadow(
                offset: Offset(3, 5),
                color: Colors.black12,
                blurRadius: 10,
              ),
              BoxShadow(
                offset: Offset(-1, -1),
                color: Colors.white,
                blurRadius: 6,
              ),
            ],
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                recentTransaction.icon,
                color: Colors.white,
              ),
              const SizedBox(width: 15.0),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    recentTransaction.title,
                    style:
                        const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600,color: Colors.white),
                  ),
                  const SizedBox(
                    height: 2.0,
                  ),
                  Text(
                    recentTransaction.dateTime,
                    style: const TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                ],
              ),
              const Spacer(),
              Text(recentTransaction.amount,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: (recentTransaction.amount.contains('-')
                          ? Colors.red
                          : Colors.green))),
            ],
          ),
        ),
      );
}
