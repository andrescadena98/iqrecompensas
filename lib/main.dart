import 'package:easy_splash_screen/easy_splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qrecompensas/pages/Principal/homePage.dart';
import 'package:qrecompensas/pages/authentication/login_page.dart';
import 'package:qrecompensas/pages/authentication/onboarding_page.dart';
import 'package:qrecompensas/pages/authentication/register_page.dart';
import 'package:qrecompensas/providers/onboarding_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

int? initScreen=0;
Future<void> main()async{
  await init();
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  initScreen = await prefs.getInt("initScreen");
  await prefs.setInt("initScreen", 1);
  print('initScreen ${initScreen}');
  runApp( MyApp());
} 
Future init() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
}
class MyApp extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => OnboardingProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home:EasySplashScreen(
          logo: Image.asset('assets/images/logo.png'),
          
          backgroundColor: Colors.white,
          showLoader: true,
          
          navigator: initScreen == 0 || initScreen == null ?OnboardingPage(): const LoginPage(),

          durationInSeconds: 3,
          ),
          
          routes: {
            'register':(context)=>const RegisterPage(),
           HomePage.routeName: (_) => HomePage(),
            OnboardingPage.routeName: (_) => OnboardingPage(),
            'login':(context)=>const LoginPage(),
            
          },
      ),
    );
  }
}