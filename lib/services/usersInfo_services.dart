import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:qrecompensas/models/users_models.dart';

class UsersServices {
  

  final url='https://dummyapi.io/data/v1/user';
 
  Future<Post> fetchPost() async {
  final response =
      await http.get(Uri.parse(url));

  if (response.statusCode==200) {
    // Si el servidor devuelve una repuesta OK, parseamos el JSON
    return Post.fromJson(json.decode(response.body));
  } else {
    // Si esta respuesta no fue OK, lanza un error.
    throw Exception('Failed to load post');
  }
}
}