import 'package:flutter/material.dart';

class Transaction {
  Transaction({
    required this.title,
    required this.dateTime,
    required this.icon,
    required this.amount,
  });

  final String title;
  final String dateTime;
  final IconData icon;
  final String amount;

  static List<Transaction> getRecentTransactions() {
    List<Transaction> recentTransactions = [
      Transaction(title: 'Pago', dateTime: '23 Feb, 8:36 PM', icon: Icons.monetization_on, amount: '-\$25'),
      Transaction(title: 'Pago', dateTime: '22 Feb, 10:12 PM', icon: Icons.monetization_on, amount: '-\$52'),
      Transaction(title: 'Pago', dateTime: '22 Feb, 9:45 AM', icon: Icons.monetization_on, amount: '\$100'),
      Transaction(title: 'Pago', dateTime: '30 Mar, 8:36 PM', icon: Icons.monetization_on,amount: '\$42'),
      Transaction(title: 'Pago', dateTime: '11 Apr, 4:26 PM', icon: Icons.monetization_on, amount: '-\$66'),
      Transaction(title: 'Pago', dateTime: '5 Sep, 3:55 AM', icon: Icons.monetization_on, amount: '\$14'),
    ];
    return recentTransactions;
  }
}