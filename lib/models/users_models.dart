class Post {
  
  final String? id;
  final String? title;
  final String? firstName;
  final String? lastName;
  final String? picture;

  Post({this.lastName, this.firstName,  this.id, this.title, this.picture});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      
      id: json['results'][0]['id'],
      title: json['results'][0]['title'],
      firstName: json['results'][0]['firstName'],
      lastName: json['results'][0]['lastName'],
      picture: json['results'][0]['picture'],
    );
  }
}